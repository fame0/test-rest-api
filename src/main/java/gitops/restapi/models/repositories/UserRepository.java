package gitops.restapi.models.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import gitops.restapi.models.db.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    public Optional<User> findByEmailAndFirstName(String email, String firstName);

    @Query("SELECT u FROM User u WHERE u.age = (SELECT max(v.age) FROM User v)")
    public Optional<User> findOldest();

    @Query("SELECT u FROM User u WHERE u.age = (SELECT min(v.age) FROM User v)")
    public Optional<User> findYoungest();

}